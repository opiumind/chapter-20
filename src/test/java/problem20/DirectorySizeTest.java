package problem20;

import junit.framework.TestCase;

import java.io.File;

/**
 * Created by Olga Lisovskaya on 9/17/17.
 */
public class DirectorySizeTest extends TestCase {
    public void testGetSize() throws Exception {
        assertEquals("Wrong size of a directory.", 11635, DirectorySize.getSize(new File("test-dir")));
        assertEquals("Wrong size of a file.", 37, DirectorySize.getSize(new File("data.txt")));
    }

}