package problem20;

import java.io.File;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

/**
 20.20 (Directory size) Rewrite Programming Exercise 18.28 using a stack instead of
 a queue.
 18.28 (Nonrecursive directory size) Rewrite Listing 18.7, DirectorySize.java, without
 using recursion.
 *
 */

public class DirectorySize {
    public static void main(String[] args) {
// Prompt the user to enter a directory or a file
        System.out.print("Enter a directory or a file: ");
        Scanner input = new Scanner(System.in);
        String directory = input.nextLine();
// Display the size
        System.out.println(getSize(new File(directory)) + " bytes");
    }


    public static long getSize(File file) {
        long size = 0; // Store the total size of all files
        Deque<File> files = new ArrayDeque<>();
        files.addFirst(file);

        while (!files.isEmpty()) {
            File currentFile = files.removeFirst();
            if (!currentFile.isDirectory()) {
                size += currentFile.length();
            } else {
                File[] directoryFiles = currentFile.listFiles(); // All files and subdirectories
                for (int i = 0; directoryFiles != null && i < directoryFiles.length; i++) {
                    files.addFirst(directoryFiles[i]);
                }
            }
        }

        return size;
    }
}
